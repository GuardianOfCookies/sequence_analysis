## Goal
- Analyse species based on the _GENBANK_ files with  predefined criteries
- Automate fetching _FASTA_ files from www.ncbi.nlm.nih.gov
## Expected input
`./Sequences/{subfamily}/*.gb`

`subfamily` folder contain selected by user species. This name is added to all rows in `./sequences.csv`
## Criteries
#### Sequences length
More than 400 (extracted from  ***REFERENCE*** attribute)
#### Coordinates area: 
- from 20,5 N >= 85 N and from 0 E >= 36 E; 
- from 27 N >= 85 N and from 36 E >= 180 E; 
- from 24 N >= 85 N and from 0 W >= 180 W
  
#### Primers  (extracted from  ***PCR_primers*** attribute)
_Not allowed primers_ 
- CAACATTTATTTTGATTTTTTGG, 
- TCCATTGCACTAATCTGCCATATTA, 
- TCCAATGCACTAATCTGCCATATTA

## Output
`/fasta/*.fasta` - extracted fasta files for each staisfied condiitons species

`sequences.fasta` - union of all fasta files

`./sequences.csv` - results of analyse of all input species with extracted attributes and status based one the suiting the conditions 
