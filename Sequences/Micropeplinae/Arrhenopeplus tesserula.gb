LOCUS       MZ633118                 658 bp    DNA     linear   INV 06-NOV-2021
DEFINITION  Arrhenopeplus tesserula voucher ZMUO.028522 cytochrome oxidase
            subunit 1 (COI) gene, partial cds; mitochondrial.
ACCESSION   MZ633118
VERSION     MZ633118.1
KEYWORDS    BARCODE.
SOURCE      mitochondrion Arrhenopeplus tesserula
  ORGANISM  Arrhenopeplus tesserula
            Eukaryota; Metazoa; Ecdysozoa; Arthropoda; Hexapoda; Insecta;
            Pterygota; Neoptera; Endopterygota; Coleoptera; Polyphaga;
            Staphyliniformia; Staphylinidae; Omaliinae group; Micropeplinae;
            Arrhenopeplus.
REFERENCE   1  (bases 1 to 658)
  AUTHORS   Roslin,T., Somervuo,P., Pentinsaari,M., Hebert,P.D.N., Agda,J.,
            Ahlroth,P., Anttonen,P., Aspi,J., Blagoev,G., Blanco,S., Chan,D.,
            Clayhills,T., deWaard,J., deWaard,S., Elliot,T., Elo,R.,
            Haapala,S., Helve,E., Ilmonen,J., Hirvonen,P., Ho,C., Itamies,J.,
            Ivanov,V., Jakovlev,J., Juslen,A., Jussila,R., Kahanpaa,J.,
            Kaila,L., Kaitila,J.P., Kakko,A., Kakko,I., Karhu,A.,
            Karjalainen,S., Kjaerandsen,J., Koskinen,J., Laasonen,E.M.,
            Laasonen,L., Laine,E., Lampila,P., Levesque-Beaudin,V., Lu,L.,
            Lahteenaro,M., Majuri,P., Malmberg,S., Manjunath,R.,
            Martikainen,P., Mattila,J., McKeown,J., Metsala,P.,
            Miklasevskaja,M., Miller,M., Miskie,R., Muinonen,A., Mukkala,V.M.,
            Naik,S., Nikolova,N., Nupponen,K., Ovaskainen,O., Osterblad,I.,
            Paasivirta,L., Pajunen,T., Parkko,P., Paukkunen,J., Penttinen,R.,
            Perez,K., Pohjoismaki,J., Prosser,S., Raekunnas,M., Rahulan,M.,
            Rannisto,M., Ratnasingham,S., Raukko,P., Rinne,A., Rintala,T.,
            Romo,S.M., Salmela,J., Salokannel,J., Savolainen,R., Schulman,L.,
            Sihvonen,P., Soliman,D., Sones,J., Steinke,C., Stahls,G.,
            Tabell,J., Tiusanen,M., Varkonyi,G., Vesterinen,E.J., Viitanen,E.,
            Vikberg,V., Viitasaari,M., Vilen,J., Warne,C., Wei,C., Winqvist,K.,
            Zakharov,E. and Mutanen,M.
  TITLE     A molecular-based identification resource for the arthropods of
            Finland
  JOURNAL   Mol Ecol Resour (2021) In press
   PUBMED   34562055
  REMARK    Publication Status: Available-Online prior to print
REFERENCE   2  (bases 1 to 658)
  AUTHORS   Roslin,T., Somervuo,P., Pentinsaari,M. and Mutanen,M.
  TITLE     Direct Submission
  JOURNAL   Submitted (07-JUL-2021) Department of Ecology, Swedish University
            of Agricultural Sciences, P.O. Box 7044, Uppsala, Uppsala SE-750
            07, Sweden
FEATURES             Location/Qualifiers
     source          1..658
                     /organism="Arrhenopeplus tesserula"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /specimen_voucher="ZMUO.028522"
                     /db_xref="BOLD:COLFH1447-16.COI-5P"
                     /db_xref="taxon:2561950"
                     /country="Finland: Regio kuusamoensis, Posio,
                     Patolamminkangas"
                     /lat_lon="66.2901 N 27.6872 E"
                     /collection_date="13-Jun-2012"
                     /collected_by="Petri Martikainen"
                     /identified_by="Mikko Pentinsaari"
                     /PCR_primers="fwd_seq: attcaaccaatcataaagatattgg, rev_seq:
                     taaacttctggatgtccaaaaaatca"
                     /PCR_primers="fwd_seq: ggtcaacaaatcataaagatattgg, rev_seq:
                     taaacttcagggtgaccaaaaaatca"
     gene            <1..>658
                     /gene="COI"
     CDS             <1..>658
                     /gene="COI"
                     /codon_start=2
                     /transl_table=5
                     /product="cytochrome oxidase subunit 1"
                     /protein_id="UDU94287.1"
                     /translation="TLYFIFGSWSGMVGTSLSMLIRAELGNPGTLIGDDQIYNVIVTA
                     HAFIMIFFMVMPIVIGGFGNWLVPLMLGAPDMAFPRMNNMSFWLLPPSLSLLLLSSMV
                     ESGAGTGWTVYPPLSSNIAHSGASVDLAIFSLHLAGISSILGAVNFITTVINMRSVGM
                     SFDRMPLFVWSVAITALLLLLSLPVLAGAITMLLTDRNLNTSFFDPAGGGDPILYQHL
                     F"
ORIGIN      
        1 aaccttatat tttatttttg gttcttgatc tggaatagtt ggaacatctt taagaatact
       61 aattcgagca gaattaggaa accctggaac attaattgga gatgatcaaa tttataatgt
      121 aattgtaact gcacatgctt tcattataat tttttttata gttataccaa ttgtaattgg
      181 aggatttgga aactgattag tacccttaat actaggagct ccagatatag ctttccctcg
      241 aataaacaat atgagatttt gacttttacc cccatcatta tctctccttt tattaagaag
      301 aatagttgaa agtggagcag gaactggttg aacagtttat cccccattat cttctaatat
      361 tgctcacaga ggggcatcag tagatttagc tatttttaga cttcatttag ctggaatttc
      421 ttcaattctt ggggcagtta attttattac tacagttatt aatatacgat cagttggaat
      481 atcatttgat cgaatgcctt tatttgtttg atcagttgct attacagcat tacttcttct
      541 tttatcatta cctgttcttg caggagcaat tactatatta ttaactgatc gaaatttaaa
      601 tacttctttt tttgatcctg ctggaggagg agatcctatt ttataccaac atttattt
//

LOCUS       HQ953526                 658 bp    DNA     linear   INV 19-APR-2019
DEFINITION  Arrhenopeplus tesserula voucher BC ZSM COL 00584 cytochrome oxidase
            subunit 1 (COI) gene, partial cds; mitochondrial.
ACCESSION   HQ953526
VERSION     HQ953526.1
DBLINK      Project: 37833
KEYWORDS    BARCODE.
SOURCE      mitochondrion Arrhenopeplus tesserula
  ORGANISM  Arrhenopeplus tesserula
            Eukaryota; Metazoa; Ecdysozoa; Arthropoda; Hexapoda; Insecta;
            Pterygota; Neoptera; Endopterygota; Coleoptera; Polyphaga;
            Staphyliniformia; Staphylinidae; Omaliinae group; Micropeplinae;
            Arrhenopeplus.
REFERENCE   1  (bases 1 to 658)
  CONSRTM   International Barcode of Life (iBOL)
  TITLE     iBOL Data Release
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 658)
  CONSRTM   International Barcode of Life (iBOL)
  TITLE     Direct Submission
  JOURNAL   Submitted (24-JAN-2011) Biodiversity Institute of Ontario,
            University of Guelph, 50 Stone Rd West, Guelph, Ontario N1G2W1,
            Canada
COMMENT     ##International Barcode of Life (iBOL)Data-START##
            Barcode Index Number :: BOLD:AAO0994
            Order Assignment     :: Coleoptera
            iBOL Working Group   :: iBOL:WG1.5
            iBOL Release Status  :: Phase 2
            ##International Barcode of Life (iBOL)Data-END##
FEATURES             Location/Qualifiers
     source          1..658
                     /organism="Arrhenopeplus tesserula"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /specimen_voucher="BC ZSM COL 00584"
                     /db_xref="BOLD:FBCOA679-10.COI-5P"
                     /db_xref="taxon:2561950"
                     /country="Belgium"
                     /lat_lon="50.7505 N 4.423 E"
                     /collection_date="28-Apr-2010"
                     /collected_by="Koehler, F."
                     /PCR_primers="fwd_seq:
                     tgtaaaacgacggccagtggtcaacaaatcataaagatattgg, rev_seq:
                     caggaaacagctatgactaaacttcagggtgaccaaaaaatca"
     gene            <1..>658
                     /gene="COI"
     CDS             <1..>658
                     /gene="COI"
                     /codon_start=2
                     /transl_table=5
                     /product="cytochrome oxidase subunit 1"
                     /protein_id="ADX11989.1"
                     /translation="TLYFIFGSWSGMVGTSLSMLIRAELGNPGTLIGDDQIYNVIVTA
                     HAFIMIFFMVMPIVIGGFGNWLVPLMLGAPDMAFPRMNNMSFWLLPPSLSLLLLSSMV
                     ESGAGTGWTVYPPLSSNIAHSGASVDLAIFSLHLAGISSILGAVNFITTVINMRSVGM
                     SFDRMPLFVWSVAITALLLLLSLPVLAGAITMLLTDRNLNTSFFDPAGGGDPILYQHL
                     F"
ORIGIN      
        1 aaccttatat tttatttttg gttcttgatc tggaatagtt ggaacatctt taagaatact
       61 aattcgagca gaattaggaa accctggaac attaattgga gatgatcaaa tttataatgt
      121 aattgtaact gcacatgctt tcattataat tttttttata gttataccaa ttgtaattgg
      181 aggatttgga aactgattag tacccttaat actaggagct ccagatatag ctttccctcg
      241 aataaacaat atgagatttt gacttttacc tccatcatta tctcttcttt tattaagaag
      301 aatagttgaa agtggagcag gaactggttg aacagtttac cccccattat cttctaatat
      361 tgctcacaga ggagcatcag tagatttagc tatttttaga cttcatttag ctggaatttc
      421 ttcaattctt ggggcagtta attttattac tacagttatt aatatacgat cagttggaat
      481 atcatttgat cgaatgcctt tatttgtttg atcagttgct attacagcat tacttcttct
      541 tttatcatta cctgttcttg caggagcaat tactatacta ttaactgatc gaaatttaaa
      601 tacttctttt tttgaccctg ctggaggagg agatcctatt ttataccaac atttattt
//

